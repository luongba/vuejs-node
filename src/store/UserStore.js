import { defineStore } from "pinia";
import { ref } from "vue";
import { setCookie, getCookie } from "@/helpers/cookie";
import api from "@/api/api";
import { useRouter } from "vue-router";
export const useUserStore = defineStore("user", () => {
    const user = ref({});
    if (getCookie("user")) {
        user.value = JSON.parse(getCookie("user"));
    }
    const router = useRouter();

    const setUser = (userParams) => {
        user.value = userParams;
        setCookie("user", JSON.stringify(userParams));
    };

    const logout = async () => {
        try {
            const response = await api.authApi.post({}, "signout");
            if (response.status === 200) {
                setCookie("token", "");
                setCookie("refreshToken", "");
                setCookie("user", "");
                user.value = {};
                router.push({ name: "signIn" });
            }
        } catch (error) {
            console.log("🚀 ~ logout ~ error:", error);
        }
    };

    const getUserInfo = async () => {
        try {
            const res = await api.authApi.actionGet("get-info-user", {});
            user.value = res.data.data;
        } catch (error) {
            logout();
        }
    };

    return { user, setUser, logout, getUserInfo };
});
