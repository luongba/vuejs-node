import { defineStore } from "pinia";
import { computed, ref } from "vue";

// You can name the return value of `defineStore()` anything you want,
// but it's best to use the name of the store and surround it with `use`
// and `Store` (e.g. `useUserStore`, `useCartStore`, `useProductStore`)
// the first argument is a unique id of the store across your application
export const useCounterStore = defineStore("counter", () => {
    const count = ref(1);

    const doubleCount = computed(() => {
        return count.value * 2;
    });

    const increment = () => {
        count.value = count.value + 1;
        console.log("🚀 ~ increment ~ count.value:", count.value)
        
    };

    return { count, doubleCount, increment };
});
