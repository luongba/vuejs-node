import { createWebHistory, createRouter } from "vue-router";
import { getCookie } from "@/helpers/cookie";
import home from "@/views/home";
import signIn from "@/views/auth/sign-in.vue";
const routes = [
    { path: "/", component: home, name: "home" },
    {
        path: "/sign-in",
        name: "signIn",
        component: signIn,
        meta: { layout: "AppLayoutBlank" },
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach(async (to, from, next) => {
    const token = getCookie("refreshToken") || getCookie("token");
    const except = ["signIn", "recovery", "forgotPassword"];
    if (!except.includes(to.name) && !token) {
        next({ name: "signIn" });
    } else {
        if (to.name === "signIn" && token) {
            next({ name: "home" });
        } else {
            next();
        }
    }

    next();
});

export default router;
