import { createApp } from "vue";
import App from "./App.vue";
import { createPinia } from "pinia";
import router from "./routes/router";
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './index.css'
const pinia = createPinia();
const app = createApp(App)
app.use(router)
app.use(pinia)
app.use(ElementPlus)
app.mount("#app");
