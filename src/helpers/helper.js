import api from "@/api/api";
import { getCookie, setCookie } from "./cookie";
export const refreshToken = async () => {
    try {
        const response = await api.authApi.post(
            {
                token: getCookie("refreshToken") || "",
            },
            "refresh-token"
        );
        if (response.data && response.data.accessToken) {
            setCookie("token", response.data.accessToken);
        }
    } catch (error) {
        console.log("🚀 ~ refreshToken ~ error:", error);
    }
};
