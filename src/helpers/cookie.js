import Cookies from "js-cookie";

export const getCookie = (name) => {
    return Cookies.get(name);
};

export const setCookie = (name, value, days = 1) => {
    Cookies.set(name, value, { expires: days, path: "" });
};
