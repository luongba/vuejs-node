import factory from "../factory";

const resource = "/auth"

export default (axios) => factory(axios)(resource)