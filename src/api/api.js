import axios from "axios";
import { getCookie } from "@/helpers/cookie";
import AuthApi from "./auth";
import { refreshToken } from "@/helpers/helper";
axios.interceptors.request.use(
    function (config) {
        const baseURL = "http://localhost:3000/";
        const token = getCookie("token");

        config.baseURL = baseURL;
        config.headers.Authorization = `Bearer ${token}`;
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    function (response) {
        return response;
    },
    async function (error) {
        if (error.response.status === 403) window.location = "/403";
        if (error.response.status === 401) {
            await refreshToken();
        }
        return Promise.reject(error);
    }
);

export default {
    authApi: AuthApi(axios),
};
