export default (axios) => (resource) => ({
    post(payload = {}, params = "", config = {}) {
        return axios.post(`${resource}/${params}`, payload, config);
    },

    index(config = {}) {
        return axios.get(`${resource}`, config);
    },

    create(payload, config = {}) {
        return axios.post(`${resource}`, payload, config);
    },

    action(id, action, payload, config = {}) {
        return axios.post(`${resource}/${id}/${action}/`, payload, config);
    },
    actionGet(action, payload, config = {}) {
        return axios.get(`${resource}/${action}/`, payload, config);
    },
    permissionGet(action, payload, config = {}) {
        return axios.get(`${resource}/${action}`, payload, config);
    },
    show(id, config = {}) {
        return axios.get(`${resource}/${id}/`, config);
    },
    showDetails(id, config = {}) {
        return axios.get(`${resource}/${id}`, config);
    },

    update(payload, id, config = {}) {
        return axios.put(`${resource}/${id}`, payload, config);
    },
    actionPut(payload, action, id, config = {}) {
        return axios.put(`${resource}/${action}/${id}`, payload, config);
    },

    updatePatch(payload, id, config = {}) {
        return axios.patch(`${resource}/${id}`, payload, config);
    },

    delete(id, config = {}) {
        return axios.delete(`${resource}/${id}`, config);
    },

    patch(id, payload, config = {}) {
        return axios.patch(`${resource}/${id}`, payload, config);
    },

    method(method = "get", path, payload, config = {}) {
        return axios[method](path, payload, config);
    },
});
